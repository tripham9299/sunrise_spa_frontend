import validator from 'validator';


const phoneFomat = /^[0-9]{9,10}$/

let required = (value, name) => {
   if (!value.toString().trim().length) {
      // We can return string or jsx as the 'error' prop for the validated Component
      return `Please enter ${name}`;
   }

   return null;

}

let address = (value) => {
   if (!value.toString().trim().length) {
      // We can return string or jsx as the 'error' prop for the validated Component
      return `Please enter the address`;
   }
   return null;

};

let firstName = (value) => {
   if (!value.toString().trim().length) {
      // We can return string or jsx as the 'error' prop for the validated Component
      return `Please enter first name`;
   }
   return null;

};


let lastName = (value) => {
   if (!value.toString().trim().length) {
      // We can return string or jsx as the 'error' prop for the validated Component
      return `Please enter last name`;
   }
   return null;

};

let confirmPassword = (value) => {
   if (!value.toString().trim().length) {
      // We can return string or jsx as the 'error' prop for the validated Component
      return `Please enter confirm password`;
   }
   return null;

};




let email = (value) => {
   if (!value.toString().trim().length) return `Please enter email`;
   if (!validator.isEmail(value)) return `The email is not in the correct format`;

   return null;

};

let phone = (value) => {
   if (!value.toString().trim().length) return `Please enter the phone number`;
   if (/[a-zA-Z]+/.test(value)) return "Phone numbers only contain numeric characters!";
   if (!phoneFomat.test(value)) return "Phone numbers are only 9 or 10 characters long!";
   return null;

}

let password = (password) => {
   if (password.length == 0) return "You have not entered password yet!";
   else if (password.length < 6) return "Password must be at least 6 characters!";
   else return "";

}

export default {
   required: required,
   email: email,
   phone: phone,
   password: password,
   firstName: firstName,
   lastName: lastName,
   address: address,
   confirmPassword: confirmPassword
}
