import { Icon } from '@material-ui/core'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import CartContext from '../../contexts/CartContext'
import './Cart.css'
import UserContext from '../../contexts/UserContext'
import cookie from 'react-cookies';

function Cart(props) {
    const config = {
        headers: {
            'Authorization': `${cookie.load('access_token')}`
        }
    };
    const [cartItems, setCartItems] = useState([])
    const [info, setInfo] = useState({
        firstName: '',
        address: '',
        lastName: '',
        phone: null
    })
    const [show, setShow] = useState(false)
    const [successConfirm, setSuccessConfirm] = useState(false)
    const { uploadCart } = useContext(CartContext)

    useEffect(() => {
        axios.get('https://sunrise-spa.herokuapp.com/cart', config)
            .then(res => {
                setCartItems(res.data.list)
                setInfo(res.data.info)
            })
    }, [])



    const updateAmount = (data) => {
        return axios.post("https://sunrise-spa.herokuapp.com/cart/updateAmount", data, config)
    }

    const handleMinus = (index) => {
        let newCartItems = [...cartItems];
        if (cartItems[index].amount > 1) {
            newCartItems[index].amount--;
        }

        updateAmount(newCartItems[index])
            .then(res => {
                setCartItems(newCartItems);
                uploadCart()
            })
            .catch((err) => {
                toast.error(err)
            })
    }


    function handleAdd(index) {
        let newCartItems = [...cartItems];
        newCartItems[index].amount++;

        updateAmount(newCartItems[index])
            .then(res => {
                setCartItems(newCartItems)
                uploadCart()
            })
            .catch((err) => {
                toast.error(err)
            })
    }

    function handleDelete(index) {
        let newCartItems = [...cartItems];
        let data = newCartItems[index]

        return axios.post("https://sunrise-spa.herokuapp.com/cart/deleteToCart", data, config)
            .then(res => {
                newCartItems.splice(index, 1);
                setCartItems(newCartItems)
                uploadCart()
            })
            .catch((e) => toast.error('Product deletion failed'))
    }

    function handleCheckOut(event) {
        event.preventDefault()
        const newInfo = {
            userName: info.lastName + info.firstName,
            address: info.address,
            phone: info.phone
        }
        console.table(newInfo)
        axios.post('https://sunrise-spa.herokuapp.com/cart/pay', newInfo, config)
            .then(response => {
                console.log(response)
                uploadCart()
                setSuccessConfirm(true)
            })
            .catch(err => {
                toast.error('An error occurred: ', err)
            })
    }

    function handleChangeInput(e, index) {
        console.log(e)
        let newCartItems = [...cartItems];
        newCartItems[index].amount = e.target.value
        setCartItems(newCartItems)
    }
    function handleShow() {
        setShow(true)
    }
    function handleClose() {
        setShow(false)
    }
    function handleForm(event) {
        let nam = event.target.name
        let val = event.target.value;
        const info1 = info;
        // console.log(info);
        setInfo({
            ...info1,
            [nam]: val
        })

    }




    var subtotal = 0, total = 0
    cartItems.map(cartItem => {
        subtotal += cartItem.product.price * cartItem.amount
    }
    )
    subtotal = Math.round(subtotal * 10) / 10
    subtotal == 0 ? total = 0 : total = subtotal + 1.5;
    const { firstName, lastName, phone, address } = info
    const user = useContext(UserContext)
    if (!user.isLogin) return <Redirect to='/login' />
    if (successConfirm) return <Redirect to="/success" />
    else if (cartItems.length > 0)
        return (
            <div>
                <div className="overlay_background_cart"  >
                    <div className='overlay_background1_cart'>
                        <h2>YOUR CART</h2>
                    </div>
                </div>
                <div className="container-fluid cart">
                    <ToastContainer />
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-8 col-md-12">
                                <table>
                                    <tbody><tr className="cart-label">
                                        <td style={{ textAlign: 'left' }}>Product</td>
                                        <td>Price</td>
                                        <td>Quantity</td>
                                        <td>Total</td>
                                        <td />
                                    </tr>
                                        {cartItems.map((cartItem, index) =>
                                            <tr className="cart-item" >
                                                <td className="card-item-first">
                                                    <div className="cart-item-image">
                                                        <img src={cartItem.product.imgUrl} alt="" />
                                                    </div>
                                                    <div className="cart-item-name">
                                                        <span>{cartItem.product.name}</span>
                                                    </div>
                                                </td>
                                                <td className="card-item-price">${cartItem.product.price}</td>
                                                <td>
                                                    <div className="cart-add-minus">
                                                        <Icon className="fa fa-minus" id="card-minus" key={index} style={{ marginRight: 10 }} onClick={() => handleMinus(index)} />
                                                        <input type="number" className="form-control col-6 col-md-3 col-lg-4" key={index} value={cartItem.amount} onChange={(e) => handleChangeInput(e, index)} />
                                                        <Icon className="fa fa-plus" id="card-add" key={index} style={{ marginLeft: 10 }} onClick={() => handleAdd(index)} />
                                                    </div>
                                                </td>
                                                <td className="card-item-total">${Math.round(cartItem.product.price * cartItem.amount * 100) / 100}</td>
                                                <td><Icon className="fa fa-trash" id="card-trash" style={{ color: 'red' }} key={index} onClick={() => handleDelete(index)} /></td>
                                            </tr>
                                        )}

                                    </tbody></table>
                            </div>
                            <div className="col-lg-4 col-6">
                                <div className="cart-right">
                                    <div className="order-summary">
                                        <legend>Order Summary</legend>
                                    </div>
                                    <div className="order-detail">
                                        <div className="subtotal">
                                            <span>Subtotal</span>
                                            <span>${subtotal}</span>
                                        </div>
                                        <div className="shipping">
                                            <span>Shipping</span>
                                            <span>$1.5</span>
                                        </div>
                                    </div>
                                    <div className="order-total">
                                        <legend>Total</legend>
                                        <span>${total}</span>
                                    </div>
                                    <div className="cart-checkout">
                                        <button className="btn btn-success btn-block" onClick={handleShow} >CHECK OUT</button>
                                    </div>
                                </div>
                            </div>
                            <Modal
                                show={show}
                                onHide={handleClose}
                                backdrop="static"
                                keyboard={false}
                                centered={true}
                            >
                                <Modal.Header>
                                    Customer Information
                        </Modal.Header>
                                <Form onSubmit={(event) => handleCheckOut(event)}>
                                    <Modal.Body>
                                        <Form.Group>
                                            <Form.Label>First name</Form.Label>
                                            <br />
                                            <Form.Control type="text" required name="firstName"
                                                placeholder="Enter first name"
                                                defaultValue={firstName}
                                                onChange={handleForm} />
                                        </Form.Group>

                                        <Form.Group>
                                            <Form.Label>Last name</Form.Label>
                                            <br />
                                            <Form.Control type="text" required
                                                name="lastName" placeholder="Enter last name"
                                                defaultValue={lastName}
                                                onChange={handleForm} />
                                        </Form.Group>

                                        <Form.Group>
                                            <Form.Label>Address </Form.Label>
                                            <br />
                                            <Form.Control type="text" required name="address"
                                                placeholder="Enter address"
                                                defaultValue={address}
                                                onChange={handleForm} />
                                        </Form.Group>

                                        <Form.Group>
                                            <Form.Label>Phone number</Form.Label>
                                            <br />
                                            <Form.Control className="col-sm-4"
                                                required type="number" name="phone"
                                                placeholder="0XXXXXX"
                                                defaultValue={phone}
                                                onChange={handleForm} />
                                        </Form.Group>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <div className="two-button">
                                            <Button variant="secondary" onClick={handleClose} style={{ marginRight: '10px' }}>
                                                Cancel
                                    </Button>
                                            <Button color="primary" type="submit" variant="primary">
                                                Confirm
                                    </Button>
                                        </div>
                                    </Modal.Footer>
                                </Form>
                            </Modal>
                        </div>
                    </div>
                </div>
            </div>
        )

    return (
        <div className="container-fluid cart"
            style={{
                height: '630px', backgroundColor: 'rgb(165, 184, 214)', display: 'flex', alignItems: 'center'
            }}>
            <ToastContainer />
            <div className="container" style={{ textAlign: 'center' }}>
                <img src="/empty.png" alt="empty" style={{ height: '80px', width: '80px' }} />
                <legend style={{ color: 'white' }}>Your shopping cart is empty</legend>
            </div>
        </div>
    )
}

export default Cart;

