import Rating from '@material-ui/lab/Rating';
import axios from 'axios';
import classnames from 'classnames';
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Col, Container, Input, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap';
import CartContext from '../../contexts/CartContext';
import "./Detail.css";
import Moment from 'react-moment';
import 'moment-timezone';
import cookie from 'react-cookies';
import Swal from 'sweetalert2'

function Detail(props) {
    const config = {
        headers: {
            'Authorization': `${cookie.load('access_token')}`
        }
    };
    const productId = props.match.params.id;

    const [product, setProduct] = useState();
    const [amount, setAmount] = useState(1)
    const cart = useContext(CartContext)

    const [related, setrelated] = useState(
        [
            {
                name: "Body Lotion",
                price: "59.00",
                img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-10-600x600.jpg"
            },
            {
                name: "Organic Bath",
                price: "99.00",
                img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-4-600x600.jpg"
            },
            {
                name: "Organic Scrub",
                price: "69.00",
                img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-2-600x600.jpg"
            }
        ]
    )

    const [activeTab, setActiveTab] = useState('1');

    useEffect(() => {
        axios.get(`https://sunrise-spa.herokuapp.com/product/${productId}`, config)
            .then(res => {
                setProduct(res.data);
            })
            .catch(err => {
                console.log(err)
            })
    }, [])

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    const addToCart = () => {
        const payload = {
            productId: productId,
            amount: parseInt(amount)
        }
        axios.post('https://sunrise-spa.herokuapp.com/cart/addToCart', payload, config)
            .then((res) => {
                cart.uploadCart()
                Swal.fire({
                    icon: 'success',
                    title: 'Added to cart successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
            })
    }
    const [dataCmt, setDataCmt] = useState([]);
    const [cmtShow, setCmtShow] = useState([]);
    const [btnShow, setBtnShow] = useState([]);
    const [show, setShow] = useState([]);
    const [addCmt, setAddCmt] = useState('')
    useEffect(() => {
        axios.get('https://sunrise-spa.herokuapp.com/comment/showComment', config)
            .then(response => {
                console.log(response.data)
                setDataCmt(response.data.allComments)
                if (response.data.user === "khach") setAddCmt('none'); else setAddCmt('block')
                let clone = [],
                    clone2 = [],
                    clone3 = []
                for (var i = 0; i < response.data.allComments.length; i++) {
                    clone[i] = 'none'
                    clone3[i] = 'block'
                    if (response.data.user === "khach") clone2[i] = 'none'; else clone2[i] = 'block'

                    if (response.data.user == response.data.allComments[i].user?._id) clone2[i] = 'block'; else clone2[i] = 'none'
                }
                setCmtShow(clone3)
                setBtnShow(clone2)
                setShow(clone)
            })

    }, []);



    const [star, setStar] = useState(5);
    const [starNew, setStarNew] = useState(5);
    const handleShow = (index) => {
        let clone = [...show]
        clone[index] = 'block'
        setShow(clone)
        let clone2 = [...cmtShow]
        clone2[index] = 'none'
        setCmtShow(clone2)
        let clone3 = [...btnShow]
        clone3[index] = 'none'
        setBtnShow(clone3)
        console.log(index)
        setStar(dataCmt[index].rate)
    };
    const handleDeleteCmt = index => {
        axios.post('https://sunrise-spa.herokuapp.com/comment/deleteComment', { cmtId: dataCmt[index]._id }, config)
            .then(response => {
                setDataCmt(response.data)
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted Successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
    }
    const handleEditCmtSubmit = index => {
        let clone = [...show]
        clone[index] = 'none'
        setShow(clone)
        let clone2 = [...cmtShow]
        clone2[index] = 'block'
        setCmtShow(clone2)
        let clone3 = [...btnShow]
        clone3[index] = 'block'
        setBtnShow(clone3)
        axios.post('https://sunrise-spa.herokuapp.com/comment/editComment', { cmtId: dataCmt[index]._id, content: dataCmt[index].content, rate: star }, config)
            .then(response => {
                setDataCmt(response.data.allComments)
                response.data.allow ?
                    Swal.fire({
                        icon: 'success',
                        title: 'Comment successfully edited',
                        showConfirmButton: false,
                        timer: 1500
                    }) :
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Comments cannot be edited!',
                    })
            })
    }
    const changeCmt = (e, index) => {
        let clone = [...dataCmt]
        clone[index].content = e.target.value
        setDataCmt(clone)
    }
    const changeNewCmt = e => {
        let clone = { ...newCmt }
        clone.content = e.target.value
        setNewCmt(clone)
    }
    const [newCmt, setNewCmt] = useState({ content: '', rate: 5 })
    const handleAddNewCmt = () => {
        axios.post('https://sunrise-spa.herokuapp.com/comment/newComment', { content: newCmt.content, rate: newCmt.rate }, config)
            .then(response => {
                setDataCmt(response.data)
                console.log(response.data)
                let clone = []
                for (var i = 0; i < response.data.length; i++) clone[i] = 'none'
                setShow(clone)
                Swal.fire({
                    icon: 'success',
                    title: 'Comment successfully added',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
    }

    const changeNewCmtRate = (newValue) => {
        setStarNew(newValue)
        let clone = { ...newCmt }
        clone.rate = newValue
        setNewCmt(clone)
    }

    return (
        <div>
            <div>
                <div className="overlay_background" style={{ backgroundImage: "url(https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/02/369127.jpg)" }} >
                    <div className='overlay_background1'>
                        <h2 style={{ fontFamily: 'Lucida Sans Unicode, Courier, monospace' }}>Detail</h2>
                    </div>
                </div>
                <Container fluid>
                    {product && <Row style={{ marginTop: "40px" }}>
                        <Col md="6" sm="6" xs="5" >
                            <div className="detail_right">
                                <img src={product.imgUrl}></img>
                            </div>
                        </Col>
                        <Col md="6" sm="6" xs="7" >
                            <div className="detail_left">
                                <h2>{product.name}</h2>
                                <h4>${product.price}</h4>
                                <p>{product.summary}</p>
                                <Input type="number" min="1"
                                    value={parseInt(amount)}
                                    onChange={(e) => setAmount(parseInt(e.target.value))}
                                    style={{ display: "inline-block", width: "75px", marginRight: '5px', textAlign: 'center' }}>

                                </Input>
                                <Button type="submit"
                                    style={{ backgroundColor: '#EFA697', fontWeight: '600', fontSize: '16px', borderColor: '#EFA697' }}
                                    onClick={addToCart}
                                >
                                    Add to card
                                 </Button>
                            </div>
                        </Col>
                        <Col md="6" xs="0"></Col>
                        <Col md="6" xs="12">
                            <Row>
                                <div className="underAdd1 col-md-12 col-sm-6 col-xs-6" ><b>SKU: 199</b></div>
                                <div className="underAdd2 col-md-12 col-sm-6 col-xs-6"><b>Category:</b> Uncategorized</div>
                                <div className="underAdd3 col-md-12 col-sm-6 col-xs-6"><b>Tags:</b> Beauty, Cream</div>
                            </Row>
                        </Col>
                    </Row>}
                    <Container>
                        <Row className="detail-description-review">

                            <div>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: activeTab === '1' })}
                                            onClick={() => { toggle('1'); }}
                                        >
                                            Description
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: activeTab === '2' })}
                                            onClick={() => { toggle('2'); }}
                                        >
                                            Review
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={activeTab}>
                                    <TabPane tabId="1">
                                        <Row>
                                            <Col sm="12">
                                                <p className="desfake">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Aliquam et elit eu nunc rhoncus viverra quis at felis et netus et malesuada fames ac turpis egestas. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <Col>
                                            {dataCmt.length !== 0 && dataCmt.map((dataComment, index) => (
                                                <Row style={{ paddingBottom: '0px' }}>
                                                    <div class="media  p-3" >

                                                        <div class="media-body" >
                                                            <div className="avt-name">
                                                                <img src="https://voxpopulii.in/system/static/dashboard/img/default_user.png" alt="John Doe" style={{
                                                                    borderRadius: "50%", maxWidth: '35px', maxHeight: '35px', marginRight: '10px', marginBottom: '5px'
                                                                }} />
                                                                <h5>{dataComment.user?.firstName}</h5>
                                                            </div>
                                                            <div key={index} style={{ display: `${cmtShow[index]}` }}>
                                                                <p style={{ marginBottom: '0px' }}> <Rating name="read-only" value={parseInt(dataComment.rate)} readOnly style={{ fontSize: '17px' }} /></p>
                                                                <p style={{ marginBottom: '0px' }}>{dataComment.content}</p>
                                                            </div>
                                                            <small><i style={{ color: 'rgb(111, 113, 115)', fontSize: '11px' }}>
                                                                <Moment format="YYYY/MM/DD HH:mm">
                                                                    {dataComment.createAt}
                                                                </Moment></i></small>
                                                            <div key={index} style={{ display: `${btnShow[index]}` }} >
                                                                <Button style={{ backgroundColor: 'transparent', color: '#007bff', border: 'none', padding: '0px', marginRight: '10px', fontWeight: '500' }} key={index} onClick={() => handleShow(index)}>Edit</Button>
                                                                <Button style={{ backgroundColor: 'transparent', color: '#dc3545', border: 'none', padding: '0px', marginLeft: '10px', fontWeight: '500' }} key={index} onClick={() => handleDeleteCmt(index)}>Delete</Button>
                                                            </div>
                                                            <div key={index} style={{ display: `${show[index]}` }} >
                                                                <Input value={dataComment.content} key={index} type="text" onChange={(e) => changeCmt(e, index)} />
                                                                <div className="btn-rt">
                                                                    <Button style={{ backgroundColor: 'transparent', color: '#007bff', border: 'none', padding: '0px', marginRight: '10px', fontWeight: '700' }} key={index} onClick={() => handleEditCmtSubmit(index)}>Comfirm</Button>
                                                                    <Rating value={star} name="simple-controlled" onChange={(event, newValue) => setStar(newValue)} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </Row>
                                            )
                                            )}
                                            <div style={{ display: `${addCmt}` }}>
                                                <Input defaultValue='' type="textarea" onChange={(e) => changeNewCmt(e)} style={{ borderRadius: '5px', maxHeight: '40px' }} />
                                                <div className="btn-rt">
                                                    <Button style={{ backgroundColor: 'transparent', color: '#007bff', border: 'none', padding: '0px', marginRight: '10px', fontWeight: '700' }} onClick={() => handleAddNewCmt()}>Comment</Button>
                                                    <Rating value={starNew} name="simple-controlled two" onChange={(event, newValue) => changeNewCmtRate(newValue)} />
                                                </div>
                                            </div>
                                        </Col>
                                    </TabPane>
                                </TabContent>
                            </div>

                        </Row>
                    </Container>


                    <hr></hr>


                    <h4 style={{ textAlign: 'center', marginTop: '40px', color: '#333333' }}>Related products</h4>

                    <Row>
                        {related.map((item, index) => (
                            <Col md="4" style={{ marginTop: '20px' }} key={index}>
                                <Link to={"/detail"} className="product">
                                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                                        <img src={item.img} alt="product"></img>
                                    </div>
                                    <h3>{item.name}</h3>
                                    <p>${item.price}</p>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default Detail;
