import axios from 'axios';
import React, { Component } from 'react';
import Moment from 'react-moment';
import 'react-toastify/dist/ReactToastify.css';
import { Button } from "reactstrap";
import LayoutTableAdmin from '../Layout/LayoutTableAdmin';
import cookie from 'react-cookies';
import Swal from 'sweetalert2'

const config = {
    headers: {
        'Authorization': `${cookie.load('access_token')}`
    }
};
export default class Bill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bills: []
        }
    }
    componentDidMount() {

        axios.get("https://sunrise-spa.herokuapp.com/bill/", config).then(res => {
            this.setState({
                bills: res.data
            });
            console.log(res.data)
        });
    }

    handleDelete = (e, index) => {
        var arr = this.state.bills;
        axios.delete(`https://sunrise-spa.herokuapp.com/bill/deleteBill/${arr[index]._id}`, config).then(res => {
            console.log(res.data)
            Swal.fire({
                icon: 'success',
                title: 'Deleted successfully',
                showConfirmButton: false,
                timer: 1500
            })
        });
        arr.splice(index, 1);
        this.setState({
            bills: arr
        });
    }
    handleEdit = (e, index) => {
        var arr = this.state.bills;
        axios.put(`https://sunrise-spa.herokuapp.com/bill/updateStatus/${arr[index]._id}`, config).then(res => {
            console.log(res.data)
        });
        arr[index].status = 1;
        this.setState({
            bills: arr
        });
    }
    render() {
        const { bills } = this.state
        return (
            <LayoutTableAdmin>
                <div>
                    <div style={{ marginBottom: "70px" }}>
                        <h2>Bills</h2>
                        <br />
                        <div className="card-2">
                            <table className="table">
                                <thead>
                                    <tr className="bill">
                                        <th >Name</th>
                                        <th >Service</th>
                                        <th >Time</th>
                                        <th className="hideUsed">Used Time</th>
                                        <th >Price</th>
                                        <th >Status</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {bills.map((bill, index) => (
                                        <tr key={index} className="bill">
                                            <td >{bill.userName}</td>
                                            <td >{bill.service.name}</td>
                                            <td ><Moment format="YYYY/MM/DD">{bill.createAt}</Moment></td>
                                            <td className="hideUsed">{bill.usedTime}</td>
                                            <td style={{ color: '#eda84a', fontWeight: '600' }}>${bill.totalMoney}</td>
                                            {bill.status === 0 && <td style={{ color: "red" }} >Unpaid</td>}
                                            {bill.status !== 0 && <td style={{ color: "green" }} >Paid</td>}
                                            <td >
                                                {bill.status !== 0 && <a href><Button className="hideButton" onClick={(e) => this.handleDelete(e, index)} style={{ backgroundColor: 'rgb(194, 12, 36)' }}><i className="fas fa-trash-alt"></i><p> Delete</p></Button></a>}
                                                {bill.status === 0 && <a href><Button className="hideButton" onClick={(e) => this.handleEdit(e, index)} style={{ backgroundColor: 'rgb(69, 189, 62)' }}><i className="fas fa-edit"></i><p> Edit</p></Button></a>}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </LayoutTableAdmin>
        )
    }
}
