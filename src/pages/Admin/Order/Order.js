import axios from 'axios';
import 'moment-timezone';
import React, { Component } from 'react';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import LayoutTableAdmin from '../Layout/LayoutTableAdmin';
import 'react-toastify/dist/ReactToastify.css';
import cookie from 'react-cookies';
import "./Order.css";
import Swal from 'sweetalert2'

const config = {
    headers: {
        'Authorization': `${cookie.load('access_token')}`
    }
};
export default class Order extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            editOrder: null,
            detailOrder: null,
            filter: 'all'
        };


    }

    componentDidMount() {
        axios.get("https://sunrise-spa.herokuapp.com/order", config).then(res => {
            this.setState({
                orders: res.data
            });

            console.table(this.state.orders)

        });

    }

    updateStatus(index, status) {
        let data = {
            _id: this.state.orders[index]._id,
            status: status
        }
        axios.put("https://sunrise-spa.herokuapp.com/order/upadteStatus", data, config).then(res => {
            let orders = this.state.orders;
            orders.splice(index, 1, res.data)
            this.setState({
                orders: orders
            })

        })
    }

    async handleStatus(e, index) {
        let { value } = e.target;
        confirmAlert({
            title: 'Change order status',
            message: 'Are you sure you want to change?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.updateStatus(index, value)
                },
                {
                    label: 'No'
                }
            ]
        });

    }


    preDetail(e, index) {
        this.setState({
            detailOrder: this.state.orders[index]
        });
    }

    handleFilter(e) {
        this.setState({
            filter: e.target.value
        })
        console.log(e.target.value)
    }

    deleteOrder(index) {
        const id = this.state.orders[index]._id
        axios.delete(`https://sunrise-spa.herokuapp.com/order/${id}`)
            .then(res => {
                let newOrders = [...this.state.orders]
                newOrders.splice(index, 1)
                this.setState({ orders: newOrders })
                Swal.fire({
                    icon: 'success',
                    title: 'Order deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
            })
    }


    render() {
        let { filter } = this.state
        let { orders, editOrder, detailOrder } = this.state;
        orders = orders.filter((e) => filter == 'all' || e.status == filter)
        var totalMoney = 0;
        return (
            <LayoutTableAdmin>


                <div class="order-admin">
                    <h2>Order</h2>
                    <br />
                    <div class="form-inline">
                        <label for="sel1">Order Status:</label> &nbsp;
                        <select class="select" class="form-control" onChange={(e) => this.handleFilter(e)} >
                            <option value='all'>All</option>
                            <option value={0}>To Ship</option>
                            <option value={1}>To Receive</option>
                            <option value={2}>Completed</option>
                            <option value={3}>Cancelled</option>
                        </select>
                    </div>
                    <br />


                    <div className="card-2">
                        <table class="table table-hover" id="">
                            <thead>
                                <tr className="orderAdmin">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th className="hideOrderAdmin">Date</th>
                                    <th className="hideOrderAdmin">Phone</th>
                                    <th>Address</th>
                                    <th>Money</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                {orders.map((order, index) => {

                                    let date = new Date(order.createAt);


                                    return (<tr className="orderAdmin">
                                        <td>{index}</td>
                                        <td>{order.userName} {order.firstName}</td>
                                        <td className="hideOrderAdmin">
                                            <Moment format="YYYY-MM-DD HH:mm">
                                                {order.createAt}
                                            </Moment>
                                        </td>
                                        <td className="hideOrderAdmin">{order.phone}</td>
                                        <td>{order.address}</td>
                                        <td>{(order.totalMoney)}</td>
                                        <td>
                                            <select class="select" class="form-control" value={order.status} onChange={(e) => this.handleStatus(e, index)} >
                                                <option value={0}>To Ship</option>
                                                <option value={1}>To Receive</option>
                                                <option value={2}>Completed</option>
                                                <option value={3}>Cancelled</option>
                                            </select>
                                        </td>

                                        <td>
                                            <form class="form-group" >
                                                <button type="button" className="btn btn-info mr-3 detail_order_admin" data-toggle="modal" data-target="#detailOrder"
                                                    onClick={(e) => this.preDetail(e, index)}><i class="fas fa-info-circle"></i><p> Detail</p></button>
                                                <button type="button" className="btn btn-danger delete_order_admin"
                                                    onClick={(e) => this.deleteOrder(index)}><i class="far fa-trash-alt"></i><p></p></button>
                                            </form>
                                        </td>
                                    </tr>
                                    )

                                })}

                            </tbody>
                        </table>
                    </div>






                    <div class="modal fade" id="detailOrder">
                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4 class="modal-title">Detail Order</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                {detailOrder &&
                                    <div class="modal-body">
                                        <p>Order ID: {detailOrder._id}</p>
                                        <p>Recipient's name: {detailOrder.userName}</p>
                                        <p>Address: {detailOrder.address}</p>
                                        <p>Order Date: <Moment format="YYYY-MM-DD HH:mm">
                                            {detailOrder.createAt}
                                        </Moment></p>
                                        <p>Phone number: {detailOrder.phone}</p>
                                        <br />


                                        <table class="table  table-inverse  table-hover">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Img</th>
                                                    <th >Name</th>
                                                    <th>Price</th>
                                                    <th>Amount</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {detailOrder.listProduct.map((element, index) => {
                                                    let product = element.product

                                                    totalMoney += product.price * element.amount;

                                                    return (<tr>
                                                        <td>{index + 1}</td>
                                                        <td>
                                                            <div className="cart-product-image">
                                                                <Link to={`/product/${product._id}`}>
                                                                    <img style={{ maxWidth: '64px' }} src={product.imgUrl} />
                                                                </Link>

                                                            </div>
                                                        </td>
                                                        <td><p className="cart-product-name">{product.name}</p></td>
                                                        <td>
                                                            <div className="cart-flex cangiua">
                                                                <p style={{ 'display': 'flex' }}>{product.price}</p>
                                                            </div>
                                                        </td>
                                                        <td><p className=" cangiua" >{element.amount}</p></td>
                                                        <td><div className="cangiua">{(product.price * element.amount)}</div></td>
                                                    </tr>)

                                                })
                                                }

                                            </tbody>

                                            <thead>
                                                <tr>
                                                    <th colspan="2">Total Money</th>
                                                    <th colspan="1">{(totalMoney)}</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>


                                                </tr>
                                            </thead>
                                        </table>

                                    </div>
                                }
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutTableAdmin>


        )
    }
}