import React, { Component } from 'react'
import axios from 'axios';
import { TextField } from "@material-ui/core";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import 'react-toastify/dist/ReactToastify.css';
import LayoutTableAdmin from '../Layout/LayoutTableAdmin';
import "./ServicesAdmin.css";
import Swal from 'sweetalert2'

export default class ServicesAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            services: [],
            isOpenModal: false,
            isOpenModal1: false,

            select: null,
            tmp: null,

            service: "",
            price: "",
            description: "",
            img: "",

            newService: "",
            newPrice: "",
            newDescription: "",
            newImg: ""

        }
    }
    componentDidMount() {
        axios.get("https://sunrise-spa.herokuapp.com/service").then(res => {
            this.setState({
                services: res.data
            });
        });
    }
    toggle = () => {
        this.setState({
            isOpenModal: !this.state.isOpenModal
        })
    }
    toggle1 = () => {
        this.setState({
            isOpenModal1: !this.state.isOpenModal1
        })
    }

    handleShowEdit = async (e, index) => {
        await this.setState({
            isOpenModal: !this.state.isOpenModal,
            tmp: index,
            select: this.state.services[index],
            service: this.state.services[index].name,
            price: this.state.services[index].price,
            description: this.state.services[index].description
        })
    }

    handleDelete = (e, index) => {
        var arr = this.state.services;
        axios.delete(`https://sunrise-spa.herokuapp.com/service/deleteService/${arr[index]._id}`).then(res => {
            Swal.fire({
                icon: 'success',
                title: 'Service deleted successfully',
                showConfirmButton: false,
                timer: 1500
            })
        });
        arr.splice(index, 1);
        this.setState({
            services: arr
        });
    }
    handleChangeEdit = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    handleEditImg = async (e) => {
        e.preventDefault();
        await this.setState({
            img: e.target.files[0]
        })
    }
    handleSubmitEdit = (e) => {
        e.preventDefault();
        const formData = new FormData()
        formData.append('img', this.state.img);
        formData.append('service', this.state.service);
        formData.append('price', this.state.price);
        formData.append('description', this.state.description);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };

        axios.put(`https://sunrise-spa.herokuapp.com/service/updateService/${this.state.select._id}`, formData, config).then(res => {
            let arr = this.state.services;
            arr[this.state.tmp].name = this.state.service;
            arr[this.state.tmp].description = this.state.description;
            arr[this.state.tmp].price = this.state.price;
            arr[this.state.tmp].img = res.data.img;

            this.setState({
                isOpenModal: !this.state.isOpenModal,
                services: arr
            })
            Swal.fire({
                icon: 'success',
                title: 'Your work has been saved',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    handleChangeNew = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    handleChangeNewImg = async (e) => {
        e.preventDefault();
        await this.setState({
            newImg: e.target.files[0]
        })
    }
    handleSubmitNewService = (e) => {
        e.preventDefault();
        const formData = new FormData()
        formData.append('img', this.state.newImg);
        formData.append('name', this.state.newService);
        formData.append('price', this.state.newPrice);
        formData.append('description', this.state.newDescription);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        axios.post("https://sunrise-spa.herokuapp.com/service/serviceCreate", formData, config).then(res => {
            let arr = this.state.services;
            console.log(res.data);
            arr.push(res.data);
            this.setState({
                isOpenModal1: !this.state.isOpenModal1,
                services: arr
            })
            Swal.fire({
                icon: 'success',
                title: 'Service added successfully',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    render() {
        const { services, select } = this.state;
        return (
            <LayoutTableAdmin>
                <div >
                    <h2>Services</h2>
                    <br />
                    <button type="button" style={{ marginBottom: '20px' }} class="btn btn-primary" data-toggle="modal" data-target="#addCategory"
                        onClick={this.toggle1}><i class="fas fa-plus"></i>&ensp;New Service</button>
                    <div className=" card-2" style={{ marginBottom: "70px" }}>
                        <table className="table">
                            <thead>
                                <tr className="serviceAdmin">
                                    <th >Img</th>
                                    <th >Service</th>
                                    <th >Description</th>
                                    <th >Price</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {services.map((item, index) => (
                                    <tr key={index} className="serviceAdmin" >
                                        <td className="imgServiceAdmin"><img src={item.img}></img></td>
                                        <td >{item.name}</td>
                                        <td className="description_service" >{item.description}</td>
                                        <td style={{ color: '#eda84a', fontWeight: '500' }}>${item.price}</td>
                                        <td >
                                            <a href><Button onClick={(e) => this.handleShowEdit(e, index)} className="editServiceAd" style={{ backgroundColor: 'rgb(69, 189, 62)', border: 'none', textAlign: "center", marginRight: '5px' }}><i className="fas fa-edit"></i></Button></a>
                                            <a href><Button onClick={(e) => this.handleDelete(e, index)} className="deleteServiceAd" style={{ backgroundColor: 'rgb(194, 12, 36)', border: 'none', textAlign: "center" }}><i className="fas fa-trash-alt"></i></Button></a>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <Modal isOpen={this.state.isOpenModal} toggle={this.toggle} >
                        <ModalHeader toggle={this.toggle}><p className="modalHeader">Edit Service</p></ModalHeader>
                        <ModalBody>
                            <form noValidate autoComplete="off">
                                {this.state.select && <TextField id="filled-basic" style={{ width: '100%' }} defaultValue={select.name} name="service" onChange={this.handleChangeEdit} label="Service" variant="filled" />}
                                {this.state.select && <TextField id="filled-basic" style={{ width: '100%' }} defaultValue={select.price} name="price" onChange={this.handleChangeEdit} label="Price" variant="filled" />}
                                {this.state.select && <TextField id="filled-basic" style={{ width: '100%' }} defaultValue={select.description} name="description" onChange={this.handleChangeEdit} label="Description" variant="filled" />}
                                <input type="file" name="img" style={{ marginTop: '12px' }} onChange={this.handleEditImg}></input>
                            </form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="success" onClick={this.handleSubmitEdit} >Submit</Button>{' '}
                            <Button color="secondary" onClick={this.toggle} >Cancel</Button>
                        </ModalFooter>
                    </Modal>




                    {/* modal new service */}
                    <Modal isOpen={this.state.isOpenModal1} toggle={this.toggle1} >
                        <ModalHeader toggle={this.toggle1}><p className="modalHeader">New service</p></ModalHeader>
                        <ModalBody>
                            <form noValidate autoComplete="off">
                                <TextField id="filled-basic" style={{ width: '100%' }} name="newService" onChange={this.handleChangeNew} label="Service" variant="filled" />
                                <TextField id="filled-basic" style={{ width: '100%' }} name="newPrice" onChange={this.handleChangeNew} label="Price" variant="filled" />
                                <TextField id="filled-basic" style={{ width: '100%' }} name="newDescription" onChange={this.handleChangeNew} label="Description" variant="filled" />
                                <input type="file" name="newImg" onChange={this.handleChangeNewImg} style={{ marginTop: '12px' }}></input>
                            </form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="success" onClick={this.handleSubmitNewService} >Submit</Button>{' '}
                            <Button color="secondary" onClick={this.toggle1} >Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </LayoutTableAdmin>
        )
    }
}
