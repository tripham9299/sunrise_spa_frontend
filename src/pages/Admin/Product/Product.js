import React, { useState, useEffect } from 'react';
import LayoutTableAdmin from '../Layout/LayoutTableAdmin';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CKEditor from "ckeditor4-react";
import './Product.css'
import Swal from 'sweetalert2'
import {
    Card, CardHeader, CardBody,
    FormGroup, Label, Input,
    Form, Row, Col
} from 'reactstrap';
import 'react-toastify/dist/ReactToastify.css';


function convertStringLong(string) {
    console.log('độ dài là ', string.length)
    if (string.length > 93) {
        let clone = string.slice(0, 93)
        return clone.concat('...')
    }
    else return string
}

function Product(props) {

    const [products, setProducts] = useState([])
    const [categories, setCategories] = useState([])
    const [newProduct, setNewProduct] = useState(null)
    const [editProduct, setEditProduct] = useState(null)
    const [formData, setFormData] = useState(null)


    useEffect(() => {
        axios.get('https://sunrise-spa.herokuapp.com/product')
            .then(res => {
                console.table(res.data)
                setProducts(res.data)
            })


    }, [])

    useEffect(() => {
        axios.get('https://sunrise-spa.herokuapp.com/category')
            .then(res => {
                console.table(res.data)
                setCategories(res.data)
            })


    }, [])

    function handleNew(e) {
        let { name } = e.target;
        let value = e.target.files ? e.target.files[0] : e.target.value
        let add = { ...newProduct };
        add[name] = value
        setNewProduct(add)

    }

    function handleAddDescription(e) {
        let add = { ...newProduct };
        add.description = e.editor.getData();
        setNewProduct(add)


    }


    function convertFormData(data) {
        let formDataNew = new FormData()
        Object.keys(data).forEach((key) => {
            formDataNew.append(key, data[key]);
            // console.log(key + "=" + data[key])

        })

        return formDataNew
    }

    function preAdd() {
        setNewProduct(null)
        if (categories && categories.length > 0)
            setNewProduct({ category: categories[0]._id })
    }

    function addProduct() {
        console.table(newProduct);
        let preForm = convertFormData(newProduct);
        axios.post("https://sunrise-spa.herokuapp.com/product", preForm)
            .then(res => {
                let productsNew = [...products];
                productsNew.push(res.data)
                setProducts(productsNew)
                Swal.fire({
                    icon: 'success',
                    title: 'Product added successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
            })

        console.table(newProduct)
    }

    function deleteProduct(index, product) {
        axios.delete(`https://sunrise-spa.herokuapp.com/product/${product._id}`)
            .then(res => {
                let productsNew = [...products]
                productsNew.splice(index, 1)
                setProducts(productsNew)
                Swal.fire({
                    icon: 'success',
                    title: 'Product deleted successfully',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
            })
    }




    function handleEditDescription(e) {
        let add = { ...newProduct };
        add.description = e.editor.getData();
        setNewProduct(add)

    }

    function handleEdit(e) {
        let { name } = e.target;
        let value = e.target.files ? e.target.files[0] : e.target.value
        let edit = { ...editProduct };
        edit[name] = value
        setEditProduct(edit)

    }


    function preEdit(index) {
        let currentProduct = { ...products[index] };
        if (currentProduct.category._id) currentProduct.category = currentProduct.category._id
        setEditProduct(currentProduct)

    }

    function onEditProduct() {
        let cloneEditProduct = { ...editProduct }
        const index = products.findIndex(product => product._id === editProduct._id)
        console.log('index', index)
        let preForm = convertFormData(cloneEditProduct);
        axios.patch(`https://sunrise-spa.herokuapp.com/product/${cloneEditProduct._id}`, preForm)
            .then(res => {
                let productsNew = [...products];
                productsNew.splice(index, 1, res.data)
                setProducts(productsNew)
                Swal.fire({
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
            })

        console.table(cloneEditProduct)
        console.log('category hiện tại', categories.find((e) => e._id == cloneEditProduct.category))
    }





    return (
        <>
            <LayoutTableAdmin>

                <div class="product-admin">

                    <h2>Product</h2>
                    <br />
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProduct"
                        onClick={(e) => preAdd()} style={{ marginBottom: '20px' }}><i class="fas fa-plus"></i>&ensp;Add Product</button>
                    <div className="card-2">
                        <table class="table table-hover " id="">
                            <thead>
                                <tr className="productAdmin">
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    {/* <th>Description</th> */}
                                    <th>Summary</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                {products.map((product, index) => {

                                    return (<tr className="row-table-custom productAdmin">
                                        <td>{index}</td>
                                        <td>
                                            {/* <Link to={`/product/${product._id}`}> */}
                                            <img className="imgProductAdmin" src={product.imgUrl} />
                                            {/* </Link> */}
                                        </td>
                                        <td>{product.name}</td>
                                        {/* <td><a href="">Detail</a></td> */}
                                        <td className="summaryProductAdmin">{convertStringLong(product.summary)}</td>

                                        <td>{(product.price)}</td>
                                        <td>
                                            <form class="form-group" >
                                                <button type="button" className="btn btn-success mr-2 editProductAdmin" data-toggle="modal" data-target="#editProduct"
                                                    onClick={(e) => preEdit(index)}><i class="far fa-edit"></i></button>
                                                <button type="button" className="btn btn-danger deleteProductAdmin"
                                                    onClick={(e) => deleteProduct(index, product)}><i class="far fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    )

                                })}

                            </tbody>

                        </table>


                    </div>

                </div>


            </LayoutTableAdmin>



            <div class="modal fade" id="addProduct">

                <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Add Product</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <form>

                                <TextField
                                    id="name"
                                    label="Name"
                                    name="name"
                                    variant="outlined"
                                    fullWidth
                                    value={newProduct?.name || ""}
                                    onChange={handleNew}
                                    autoFocus
                                    margin="normal"
                                />

                                <TextField
                                    id="price"
                                    label="Price"
                                    name="price"
                                    type="number"
                                    variant="outlined"
                                    fullWidth
                                    value={newProduct?.price || ""}
                                    onChange={handleNew}
                                    margin="normal"
                                />

                                <TextField
                                    id="summary"
                                    label="Summary"
                                    name="summary"
                                    multiline
                                    rowsMax={4}
                                    variant="outlined"
                                    fullWidth
                                    value={newProduct?.summary || ""}
                                    onChange={handleNew}
                                    margin="normal"
                                />

                                <div class="form-group">
                                    <label for="category">Category:</label>
                                    <select class="form-control" id="category" name="category"
                                        onChange={handleNew}
                                        value={newProduct?.category}
                                    >
                                        {categories.map((category, index) => (<option value={category._id} >{category.name}</option>))}

                                    </select>
                                </div>


                                <FormGroup>
                                    <Label for="imgUrl">Chọn Ảnh</Label>
                                    <Input
                                        onChange={handleNew}
                                        type="file" name="imgUrl"
                                        placeholder="chọn ảnh" />
                                </FormGroup>





                                <label htmlFor="contained-button-file">
                                    Description
				        <CKEditor data={newProduct?.description || ""} name="description" onChange={handleAddDescription} />
                                </label>

                            </form>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onClick={addProduct} data-dismiss="modal">Add</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>



            <div class="modal fade" id="editProduct">
                <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Edit Product</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            {editProduct &&
                                <form>

                                    <TextField
                                        id="name"
                                        label="Name"
                                        name="name"
                                        variant="outlined"
                                        fullWidth
                                        value={editProduct.name}
                                        onChange={handleEdit}
                                        autoFocus
                                        margin="normal"
                                    />

                                    <TextField
                                        id="price"
                                        label="Price"
                                        name="price"
                                        type="number"
                                        variant="outlined"
                                        fullWidth
                                        value={editProduct.price}
                                        onChange={handleEdit}
                                        margin="normal"
                                    />

                                    <TextField
                                        id="summary"
                                        label="Summary"
                                        name="summary"
                                        multiline
                                        rowsMax={4}
                                        variant="outlined"
                                        fullWidth
                                        value={editProduct.summary}
                                        onChange={handleEdit}
                                        margin="normal"
                                    />

                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select class="form-control" id="category" name="category"
                                            onChange={handleEdit}
                                            value={editProduct?.category || editProduct?.category._id}
                                        >
                                            {categories.map((category, index) => (<option value={category._id} >{category.name}</option>))}

                                        </select>
                                    </div>





                                    <FormGroup>
                                        <Label for="imgUrl">Chọn Ảnh</Label>
                                        <Input
                                            onChange={handleEdit}
                                            type="file" name="imgUrl"
                                            placeholder="chọn ảnh" />
                                    </FormGroup>
                                    <label htmlFor="contained-button-file">
                                        Description
				                <CKEditor name="description" data={editProduct?.description || null} onChange={handleEditDescription} />
                                    </label>
                                </form>
                            }


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onClick={onEditProduct} data-dismiss="modal"><i class="far fa-save"></i>&ensp;Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>



        </>
    );
}

export default Product;