import React, { Component, useState, useEffect, useContext } from 'react'
import './Profile.css'
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { ToastContainer, toast } from 'react-toastify';
import UserContext from '../../../contexts/UserContext';
import cookie from 'react-cookies';
import { Spinner } from "reactstrap";
import Swal from 'sweetalert2'


function Profile(props) {
    const [state, setState] = useState({ user: null })
    const userContext = useContext(UserContext)

    useEffect(() => {
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        axios.get('https://sunrise-spa.herokuapp.com/user/profile', config)
            .then(res => {
                setState({
                    user: res.data
                })
            })
            .catch(err => {
                console.log(err)
            })

    }, [])


    function handleEdit(e) {
        let { name, value } = e.target;

        let newEdit = { ...state.user };
        newEdit[name] = value

        setState({
            user: newEdit
        });

        console.table(state.user)
    }


    function updateProfile() {
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        let { user } = state;
        if (user.email) delete user.email
        if (user.avatar) delete user.avatar
        axios.patch("https://sunrise-spa.herokuapp.com/user/profile", user, config)
            .then(res => {
                setState({
                    user: res.data
                });
                Swal.fire({
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
                let firstNameClient = localStorage?.firstName;
                if (firstNameClient != user.firstName) {
                    localStorage.setItem('firstName', user.firstName)
                    userContext.setFirstName(user.firstName)
                }
            })
            .catch((err) => toast.error(err))
    }


    const { user } = state

    return (
        <div class="user-admin" style={{ backgroundImage: "url(../city.webp)" }}>
            <br />
            <ToastContainer />
            <div className="card profile-card col-sm-8 col-md-8 col-lg-6" >
                <h2 style={{ 'margin': '20px' }}>Profile</h2>

                {user &&
                    <form style={{ 'margin': '20px' }}>


                        <TextField
                            id="email_profile"
                            label="Email"
                            name="email"
                            variant="filled"
                            fullWidth
                            value={user?.email}
                            // onChange={handleEdit.bind(this)}
                            margin="normal"
                            inputProps={
                                { readOnly: true, }
                            }
                        />

                        <TextField
                            id="firstName_profile"
                            label="First Name"
                            name="firstName"
                            variant="outlined"
                            fullWidth
                            value={user?.firstName}
                            onChange={handleEdit.bind(this)}
                            margin="normal"
                        />

                        <TextField
                            id="lastName_profile"
                            label="Last Name"
                            name="lastName"
                            variant="outlined"
                            fullWidth
                            value={user?.lastName}
                            onChange={handleEdit.bind(this)}
                            margin="normal"
                        />





                        <TextField
                            id="address_profile"
                            label="Address"
                            name="address"
                            variant="outlined"
                            fullWidth
                            value={user?.address}
                            onChange={handleEdit.bind(this)}
                            margin="normal"
                        />

                        <TextField
                            id="phone_profile"
                            label="Phone"
                            name="phone"
                            variant="outlined"
                            fullWidth
                            value={user?.phone}
                            onChange={handleEdit.bind(this)}
                            margin="normal"
                        />


                        <button type="button" class="btn btn-outline-primary" style={{ marginTop: '10px' }} onClick={updateProfile}
                        >
                            <i class="fa fa-save"></i>&ensp;Update
                            </button>

                    </form>
                }
                {user == null && <Spinner style={{ margin: 'auto' }} color="light" />}
            </div>

        </div>

    );
}

export default Profile;

