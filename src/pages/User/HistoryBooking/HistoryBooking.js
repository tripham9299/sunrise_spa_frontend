import axios from 'axios';
import React, { Component } from 'react';
import Moment from 'react-moment';
import { Button } from "reactstrap";
import "./HistoryBooking.css";
import cookie from 'react-cookies';
import { Spinner } from "reactstrap";
import Swal from 'sweetalert2'

export default class HistoryBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bills: []
        }

    }
    componentDidMount() {
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        axios.get("https://sunrise-spa.herokuapp.com/bill/billUser", config).then(res => {
            this.setState({
                bills: res.data
            });
        });
    }

    handleDelete = (e, index) => {
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        var arr = this.state.bills;
        axios.delete(`https://sunrise-spa.herokuapp.com/bill/deleteBill/${arr[index]._id}`, config).then(res => {
            console.log(res.data)
            Swal.fire({
                icon: 'success',
                title: 'Deleted successfully',
                showConfirmButton: false,
                timer: 1500
            })
        });
        arr.splice(index, 1);
        this.setState({
            bills: arr
        });
    }
    render() {
        const { bills } = this.state
        return (
            <div>
                <div className="overlay_background" style={{ backgroundImage: "url(https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/02/369127.jpg)" }} >
                    <div className='overlay_background1'>
                        <h2>History Booking</h2>
                    </div>
                </div>
                <div className="container-fluid card-2" style={{ marginBottom: "250px" }}>
                    <table className="table">
                        <thead>
                            <tr className="bill">
                                <th >Name</th>
                                <th>Service</th>
                                <th >Time</th>
                                <th className="hideUsed">Used Time</th>
                                <th >Price</th>
                                <th >Status</th>
                                <th >Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            {bills.map((bill, index) => (

                                <tr key={index} className="bill">
                                    <td >{bill.userName}</td>
                                    <td >{bill.service.name}</td>
                                    <td ><Moment format="YYYY/MM/DD">{bill.createAt}</Moment></td>
                                    <td className="hideUsed" >{bill.usedTime}</td>
                                    <td style={{ color: '#eda84a', fontWeight: '700' }}>${bill.totalMoney}</td>
                                    {bill.status === 0 && <td style={{ color: "red" }} >Unpaid</td>}
                                    {bill.status !== 0 && <td style={{ color: "green" }} >Paid</td>}
                                    <td >
                                        {/* <Button className="hideButton" style={{ backgroundColor: '#17a2b8' }}><i className="fas fa-info-circle"></i><p>Detail</p> </Button> */}
                                        {bill.status !== 0 && <a href><Button className="hideButton" onClick={(e) => this.handleDelete(e, index)} style={{ backgroundColor: 'rgb(194, 12, 36)' }}><i className="fas fa-trash-alt"></i><p> Delete</p></Button></a>}
                                        {bill.status === 0 && <a href><Button className="hideButton" onClick={(e) => this.handleDelete(e, index)} style={{ backgroundColor: 'rgb(218, 84, 46)' }}><i className="far fa-window-close"></i><p> Cancel</p></Button></a>}

                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                    {bills == "" && <div className="spinnerBooking" ><Spinner style={{ marginTop: '80px', marginBottom: '85px' }} color="green" /></div>}
                </div>
            </div>
        )
    }
}
