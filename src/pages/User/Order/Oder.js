import React, { Component } from 'react'
import './Order.css'
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CKEditor from "ckeditor4-react";
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import cookie from 'react-cookies';
import { Spinner } from "reactstrap";

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';

export default class Order extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            filter: 'all',
            editOrder: null,
            detailOrder: null
        };


    }

    componentDidMount() {
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        axios.get("https://sunrise-spa.herokuapp.com/order/user", config).then(res => {
            this.setState({
                orders: res.data
            });


        });

    }


    preDetail(e, index) {
        this.setState({
            detailOrder: this.state.orders[index]
        });
    }

    handleFilter(filter) {
        this.setState({
            filter: filter
        })
    }

    handleCanel(index) {

        console.log(index)
        confirmAlert({
            title: 'Change order status',
            message: 'Are you sure you want to change?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.Cancel(index)
                },
                {
                    label: 'No'
                }
            ]
        });

    }

    Cancel(index) {
        let data = {
            _id: this.state.orders[index]._id,
            status: 3
        }
        const config = {
            headers: {
                'Authorization': `${cookie.load('access_token')}`
            }
        };
        axios.put("/order/upadteStatus", data, config).then(res => {
            let orders = this.state.orders;
            orders.splice(index, 1, res.data)
            this.setState({
                orders: orders
            })

        })
    }









    render() {

        const { orders, editOrder, detailOrder, filter } = this.state;
        var totalMoney = 0;
        const filterOrders = orders.filter((e) => filter == 'all' || e.status == filter)
        return (

            <div>
                <div className="overlay_background" style={{ backgroundImage: "url(https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/02/369127.jpg)" }} >
                    <div className='overlay_background1'>
                        <h2>History Order</h2>
                    </div>
                </div>
                <div class="order-admin container-fluid" style={{ marginTop: "5px", marginBottom: '210px' }}>
                    <div>
                        <div className="select-menu-order">
                            <ul class="nav nav-tabs nav-justified">
                                <li className={classNames('nav-item', { 'active': filter == 'all' })}>
                                    <span onClick={() => this.handleFilter('all')} class="nav-link " href="#">All {filter == 'all' && `(${filterOrders.length})`} </span>
                                </li>
                                <li className={classNames('nav-item', { 'active': filter == 0 })}>
                                    <span onClick={() => this.handleFilter(0)} class="nav-link" href="#">To Ship {filter == 0 && `(${filterOrders.length})`} </span>
                                </li>
                                <li className={classNames('nav-item', { 'active': filter == 1 })}>
                                    <span onClick={() => this.handleFilter(1)} class="nav-link" href="#">To Receive {filter == 1 && `(${filterOrders.length})`} </span>
                                </li>
                                <li className={classNames('nav-item', { 'active': filter == 2 })}>
                                    <span onClick={() => this.handleFilter(2)} class="nav-link" href="#">Completed  {filter == 2 && `(${filterOrders.length})`} </span>
                                </li>
                                <li className={classNames('nav-item', { 'active': filter == 3 })} >
                                    <span onClick={() => this.handleFilter(3)} class="nav-link" href="#">Cancelled  {filter == 3 && `(${filterOrders.length})`} </span>
                                </li >
                            </ul >
                        </div >

                        <br />

                        <div className="card-2">
                            <table class="table">
                                <thead>
                                    <tr className="bill">
                                        <th>ID</th>
                                        {/* <th>Name</th> */}
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Money</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {filterOrders.map((order, index) => {

                                        return (<tr className="bill">
                                            <td>{index}</td>
                                            {/* <td>{order.userName} {order.firstName}</td> */}
                                            <td>{order.phone}</td>
                                            <td>{order.address}</td>
                                            <td>{order.totalMoney}</td>

                                            {order.status == 0 && <td>To Ship</td>}
                                            {order.status == 1 && <td>To Receive</td>}
                                            {order.status == 2 && <td>Completed</td>}
                                            {order.status == 3 && <td>Cancelled</td>}


                                            <td>
                                                <form class="form-group" >
                                                    <button type="button" className="btn btn-info button_detail" data-toggle="modal" data-target="#detailOrder"
                                                        onClick={(e) => this.preDetail(e, index)}><i className="fas fa-info-circle"></i></button>

                                                    {order.status === 0 && <button type="button" class="btn btn-danger button_cancel"
                                                        onClick={() => this.handleCanel(index)}><i className="fas fa-window-close"></i></button>}
                                                </form>
                                            </td>
                                        </tr>
                                        )

                                    })}

                                </tbody>
                            </table>
                            {filterOrders == "" && <div className="spinnerBooking" ><Spinner style={{ marginTop: '80px', marginBottom: '85px' }} color="green" /></div>}
                        </div>






                        <div class="modal fade" id="detailOrder">
                            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Detail Order</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    {detailOrder
                                        &&
                                        <div class="modal-body">
                                            <p>Order ID: {detailOrder._id}</p>
                                            <p>Recipient's name: {detailOrder.userName}</p>
                                            <p>Address: {detailOrder.address}</p>
                                            <p>Phone number: {detailOrder.phone}</p>
                                            <br />


                                            <table class="table  table-inverse  table-hover">
                                                <thead>
                                                    <tr className="detail_title">
                                                        <th>STT</th>
                                                        <th>Img</th>
                                                        <th >Name</th>
                                                        <th>Price</th>
                                                        <th>Amount</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {detailOrder.listProduct.map((element, index) => {
                                                        let product = element.product

                                                        totalMoney += product.price * element.amount;

                                                        return (<tr className="detail_content">
                                                            <td>{index + 1}</td>
                                                            <td>
                                                                <div className="cart-product-image">
                                                                    <Link to={`/product/${product._id}`}>
                                                                        <img style={{ maxWidth: '64px' }} src={product.imgUrl} />
                                                                    </Link>
                                                                </div>


                                                            </td>
                                                            <td><p className="cart-product-name">{product.name}</p></td>
                                                            <td>
                                                                <div className="cart-flex cangiua">
                                                                    <p style={{ 'display': 'flex' }}>{product.price}</p>
                                                                </div>
                                                            </td>
                                                            <td><p className=" cangiua" >{element.amount}</p></td>
                                                            <td><div className="cangiua">{product.price * element.amount}</div></td>
                                                        </tr>)

                                                    })
                                                    }

                                                </tbody>

                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Total Money</th>
                                                        <th colspan="1">{totalMoney}</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>

                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    }
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >
                </div >

            </div>
        )
    }
}