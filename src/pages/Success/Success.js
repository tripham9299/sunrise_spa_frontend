import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Success.css'
export default class Success extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className="successCheckout" style={{ marginBottom: '60px' }}>
                <div className="circle">
                    <div className="circleContent">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>
                <h1>The order has been successfully created</h1>
                <Link className="back-to-home" to="/">Back to home </Link>
            </div>
        )
    }
}