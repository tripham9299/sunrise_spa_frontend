import { Slider, Typography } from '@material-ui/core';
import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Col, Container, Row } from 'reactstrap';
import "./Shop.css";


class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [

            ],
            popular: [
                {
                    name: "Body Lotion",
                    price: "59.00",
                    img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-10-600x600.jpg"
                },
                {
                    name: "Organic Bath",
                    price: "99.00",
                    img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-4-600x600.jpg"
                },
                {
                    name: "Organic Scrub",
                    price: "69.00",
                    img: "https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/06/product-2-600x600.jpg"
                }
            ],
            value: [0, 200],
            start: 0,
            end: 200,
            count: null,

            // search: [],
            key: null,
            productSearch: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount = async () => {
        axios.get('https://sunrise-spa.herokuapp.com/product')
            .then(res => {
                console.log(res.data.length)
                this.setState({
                    products: res.data,
                    count: res.data.length,
                    productSearch: res.data
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleChangeSearch = (e) => {
        let tmp = e.target.value
        this.setState({
            key: tmp
        })
    }
    handleClickSearch = () => {
        let ite = this.state.key;
        console.log(ite);

        var tmp1 = this.state.productSearch.filter(function (s) {
            return s.name.toLowerCase().indexOf(ite.toLowerCase()) !== -1;
        });
        console.log(tmp1.length);
        this.setState({
            products: tmp1,
            count: tmp1.length
        })
    }

    handleChange(e, newValue) {
        this.setState({
            value: newValue
        })
    }
    handleClick(e) {
        this.setState({
            start: this.state.value[0],
            end: this.state.value[1],
            count: this.state.dem
        })
        let tmp = 0;
        this.state.products.forEach(item => {
            if (item.price >= this.state.value[0] && item.price <= this.state.value[1]) {
                tmp++;
            }
        });
        this.setState({
            count: tmp
        })
    }
    render() {
        const { products, popular, search } = this.state;
        return (
            <div>
                <div className="overlay_background" style={{ backgroundImage: "url(https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandspa/demo/wp-content/uploads/2017/02/369127.jpg)" }} >
                    <div className='overlay_background1'>
                        <h2>SHOP</h2>
                    </div>
                </div>
                <Container fluid>
                    <Container>
                        <Row>
                            <Col lg="4">
                                <div style={{}}>
                                    <Typography id="range-slider" gutterBottom style={{ marginTop: '40px', fontWeight: '700' }}>
                                        FILTER BY PRICE
                                    </Typography>
                                    <Slider
                                        min={0}
                                        max={200}
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        valueLabelDisplay="auto"
                                        aria-labelledby="range-slider"
                                        // getAriaValueText={}
                                        style={{ width: "100%", color: "#444", display: 'block' }}
                                    />
                                    <Button type="submit" onClick={this.handleClick} style={{ display: 'inline-block', color: '#515151', backgroundColor: '#ebe9eb', fontSize: '15px', borderColor: '#ebe9eb' }}>FILTER</Button>
                                    <b style={{ display: 'inline-block', float: 'right', fontSize: '14px', fontWeight: '700', marginTop: '10px' }}>Price:${this.state.value[0]}-{this.state.value[1]}</b>
                                </div>
                            </Col>
                            <Col lg="5">

                            </Col>
                            <Col lg="3">

                                <div class="row no-gutters" style={{ marginTop: '30px' }}>
                                    <div class="col">
                                        <input class="form-control border-secondary border-right-0 rounded-0" onChange={this.handleChangeSearch} type="search" id="example-search-input4"></input>
                                    </div>
                                    <div class="col-auto">
                                        <button class="btn btn-outline-secondary border-left-0 rounded-0 rounded-right" type="submit" onClick={this.handleClickSearch}>
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>


                                <div >Showing all {this.state.count} results</div>
                            </Col>
                        </Row>
                    </Container>


                    <Row>
                        {products.map((product, index) => (
                            product.price >= this.state.start && product.price <= this.state.end &&
                            <Col md="4" sm="6" xs="6" style={{ marginTop: '20px' }} key={index}>
                                <Link to={`/product/${product._id}`} className="product">
                                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                                        <img src={product.imgUrl} alt="product"></img>
                                    </div>
                                    <h3>{product.name}</h3>
                                    <p>${product.price}</p>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    <hr></hr>
                    <h4 style={{ textAlign: 'center', marginTop: '40px', color: '#333333' }}>Popular</h4>
                    <Row>
                        {popular.map((item, index) => (
                            <Col md="4" style={{ marginTop: '20px' }} key={index}>
                                <Link to={`/product/${item._id}`} className="product">
                                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                                        <img src={item.img} alt="product"></img>
                                    </div>
                                    <h3>{item.name}</h3>
                                    <p>${item.price}</p>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Shop;
