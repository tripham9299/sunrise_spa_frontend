import React, { useEffect, useState } from 'react';
import cookie from 'react-cookies'
import axios from 'axios';


const UserContext = React.createContext();
export default UserContext
export const UserConsumer = UserContext.Consumer;

export function UserProvider(props) {

    const [isLogin, setIsLogin] = useState(false)
    const [firstName, setFirstName] = useState('')
    const [isAdmin, setIsAdmin] = useState(localStorage.getItem('role'))

    function Logout() {
        setIsLogin(false);
        setFirstName('');
        localStorage.removeItem('firstName')
        localStorage.removeItem('role')

        axios.post('https://sunrise-spa.herokuapp.com/auth/logout')
            .then(res => {
                console.log('xoá token thành công')
            })
            .catch(err => {
                console.log('xoá cookie thất bại')
            })

        cookie.remove('refresh_token')
        cookie.remove('access_token')
        window.location.href = "/"

    }

    useEffect(() => {
        if (localStorage.firstName && localStorage.firstName != 'undefined') {
            setIsLogin(true);
            setFirstName(localStorage.firstName)
        }
        else {
            setIsLogin(false);
            localStorage.removeItem('firstName')
        }
    }, [])


    return (
        <UserContext.Provider value={{
            isLogin,
            firstName,
            setIsLogin,
            setFirstName,
            Logout,
            isAdmin,
            setIsAdmin
        }}>
            {props.children}
        </UserContext.Provider>
    );
}

