import React, { Component, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import CartContext from '../../contexts/CartContext';
import UserContext from '../../contexts/UserContext';
import './Header.css';


function Header(props) {
	const user = useContext(UserContext)
	const cart = useContext(CartContext)
	return (
		<Header1 user={user} cart={cart} />
	);
}

export default Header;

class Header1 extends Component {

	constructor(props) {
		super(props);
		this.state = {
			scrolling: 'noscroll',
			oldscroll: 0
		}
		this.handleScroll = this.handleScroll.bind(this)
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	async handleScroll() {
		// let scrollTop = event.srcElement.body.scrollTop,
		//     itemTranslate = Math.min(0, scrollTop/3 - 60);

		// this.setState({
		//   transform: itemTranslate
		// });
		let y = window.scrollY;

		if (y > 0 && y > this.state.oldscroll) this.setState({
			scrolling: 'none'
		})
		else if (y === 0 && this.state.scrolling !== 'noscroll') this.setState({
			scrolling: 'noscroll'
		})
		else if (y > 0 && this.state.scrolling !== 'scroll') this.setState({
			scrolling: 'scroll'
		})
		await this.setState({ oldscroll: y })

	}
	render() {
		var classScroll = this.state.scrolling;
		const { user, cart } = this.props

		return (
			<div>
				<div className='header'>
					<div className={`big-menu ${classScroll}`}>
						<div>
							<div className="menu">
								<nav class="menu1 navbar navbar-expand-lg ">
									{classScroll === 'noscroll' && <Link className="navbar-brand" to="/"><img className="repologo" src="/icon/header/logoWhite.png" alt="logo" width={120} height={38} style={{ position: 'relative', top: "-6px" }} /></Link>}
									{classScroll === 'scroll' && <Link className="navbar-brand" to="/"><img className="repologo" src="/icon/header/logoBlack.png" alt="logo" width={120} height={38} style={{ position: 'relative', top: "-6px" }} /></Link>}
									<button className="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
										<i className="fa fa-bars icon-humbuger"></i>
									</button>

									<div className="collapse navbar-collapse " id="navbarNav">
										<ul className="navbar-nav">
											<li className="nav-item">
												<NavLink className="nav-link" activeClassName="selected" exact to="/">HOME</NavLink>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/booking">BOOKING</Link>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/services">SERVICES</Link>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/shop">SHOP</Link>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/about">ABOUT US</Link>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/contact">CONTACT</Link>
											</li>
											<li className="nav-item">
												<Link className="nav-link" to="/gallery">GALLERY</Link>
											</li>
											{user.isAdmin == 2 && <li className="nav-item">
												<Link className="nav-link" to="/admin/home">ADMIN</Link>
											</li>}
										</ul>
									</div>
								</nav>
							</div>
						</div>


						<div className="logo-right">
							<Link to="/booking" className="text-book-menu">BOOK</Link>
							{/* <a href="/" >BOOK</a> */}

							<Link className="div0" to="/cart">
								<div className="div1">
									<div class="header-cart-icon">
										<div className="div2"><i className="fa fa-cart-plus icon-cart"></i></div>

										{cart.cartAmount > 0 && <div className="icon-cart-quantity">
											<span>{cart.cartAmount}</span>
										</div>}



									</div>
								</div>
							</Link>

							{!user.isLogin && <div className="menu-log">
								<Link className="menu-login" to="/login">Login</Link>
							</div>
							}


							{user.isLogin && <>
								<Link className="nav-link dropdown-toggle" to="/" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Hello {user.firstName}
								</Link>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<Link className="dropdown-item" to="/user/profile"><i className="fas fa-user-edit"></i>&nbsp; Profile</Link>
									<Link className="dropdown-item" to="/user/changepass"><i className="fas fa-key"></i>&nbsp; Change Password</Link>
									<Link className="dropdown-item" to="/user/historyBooking"><i className="fas fa-bookmark"></i>&nbsp; History Booking</Link>
									<Link className="dropdown-item" to="/user/order"><i className="fas fa-history"></i>&nbsp; History Order</Link>
									<Link className="dropdown-item" to="/" onClick={() => user.Logout()}><i className="fas fa-sign-out-alt"></i>&nbsp; Logout</Link>
								</div>
							</>
							}




						</div>

					</div>
				</div>
			</div>
		);
	}
}
